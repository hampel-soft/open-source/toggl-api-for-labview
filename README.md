# Toggl API for LabVIEW

A collection of LabVIEW VIs for facilitating Toggl's API.

## :bulb: Documentation

* https://github.com/toggl/toggl_api_docs
* https://dokuwiki.hampel-soft.com/code/open-source/toggl-api-for-labview


## :rocket: Installation

Download the latest VIP file from https://gitlab.com/hampel-soft/open-source/toggl-api-for-labview/-/releases

### :wrench: LabVIEW 2014

The VIs are maintained in LabVIEW 2014.

### :link: Dependencies

Apply the `toggl-api-for-labview.vipc` file to install the needed VI packages.


## :busts_in_silhouette: Contributing 

Please take a look at the [issue tracker](https://gitlab.com/hampel-soft/open-source/toggl-api-for-labview/issues) 
for a list of potential tasks.

Our Dokuwiki holds more information on [how to collaborate](https://dokuwiki.hampel-soft.com/processes/collaboration) 
on an open-source project, in case you need help with the processes. Please [get 
in touch with us](office@hampel-soft.com) for any further questions.

##  :beers: Credits

The VIs in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com). 

Contributors:
* Joerg Hampel

## :page_facing_up: License 

This repository and its contents are licensed under a BSD/MIT like license - see the [LICENSE](LICENSE) file for details
