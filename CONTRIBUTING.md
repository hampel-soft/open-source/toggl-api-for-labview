# Contributing to the HSE Open-Source Projects

You can find more details about collaborating/contributing [on our Dokuwiki](https://dokuwiki.hampel-soft.com/processes/collaboration).

By submitting code as an individual you agree to the [Individual Fiduciary Contributor License Agreement](https://dokuwiki.hampel-soft.com/_media/code/common/fiduciary-license-license-agreement-2.0.pdf).

By submitting code as an entity you agree to the [Entity Fiduciary Contributor License Agreement](https://dokuwiki.hampel-soft.com/_media/code/common/entity-fiduciary-license-license-agreement-2.0.pdf).
